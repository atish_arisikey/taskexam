<?php
defined('BASEPATH') or exit('No direct script access allowed');

class QuizController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['page_title'] = 'Quiz | Task Exam';
        $data['page'] = 'quiz_view';
        $this->load->view($data['page'], $data);
    }

    public function getAllData()
    {
        $response = [
            'status' => 1,
            'message' => 'Ooh! Somthing happened terrible.',
            'data' => '',
        ];

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method === "GET") {
    
            $data = $this->QuizModel->getAllData();

            if ($data) {
                $response = [
                    'status' => 0,
                    'message' => 'Success',
                    'data' => $data
                ];
            } else {
                $response = [
                    'status' => 1,
                    'message' => 'Record not found',
                    'data' => null
                ];
            }
        } else {
            $response = [
                'status' => 1,
                'message' =>  'Request method cannot be accepted.',
                'data' => null,
            ];
        }

        echo json_encode($response);
    }
}
