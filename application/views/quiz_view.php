<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quiz</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?= ASSETS_URL ?>css/adminLTE/css/bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= ASSETS_URL ?>css/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <!-- Custome style -->
  <link rel="stylesheet" href="<?= ASSETS_URL ?>css/adminLTE/css/custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');

    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box
    }

    body {
      background-color: #333
    }

    .container {
      background-color: #555;
      color: #ddd;
      border-radius: 10px;
      padding: 20px;
      font-family: 'Montserrat', sans-serif;
      max-width: 700px
    }

    .container>p {
      font-size: 32px
    }

    .container button:hover {
      background: #36a39c;
      color: #FFF;
    }

    .container button:disabled {
      opacity: 0.5;
      background: #9ACFCC;
      color: #00403C;
      cursor: default;
    }

    #question {
      width: 79%
    }

    #question input[type=radio] {
      display: none;
    }

    #question label {
      display: inline-block;
      margin: 4px;
      padding: 8px;
      background: #ffffff;
      color: #060606;
      width: calc(50% - 8px);
      min-width: 129px;
      cursor: pointer;
    }

    #question label:hover {
      background-color: #21bf73;
    }

    #question input[type=radio]:checked+label {
      background: #CB8306;
      color: #FAFAFA;
    }

    #quiz-results {
      display: flex;
      flex-direction: column;
      justify-content: center;
      position: absolute;
      top: 44px;
      left: 0px;
      background: #FAFAFA;
      width: 100%;
      height: calc(100% - 44px);
    }

    #quiz-results-message {
      display: block;
      color: #00403C;
      font-size: 20px;
      font-weight: bold;
    }

    #quiz-results-score {
      display: block;
      color: #31706c;
      font-size: 20px;
    }

    #quiz-results-score b {
      color: #00403C;
      font-weight: 600;
      font-size: 20px;
    }


    .btn-primary {
      background-color: #555;
      color: #ddd;
      border: 1px solid #ddd
    }

    .btn-primary:hover {
      background-color: #21bf73;
      border: 1px solid #21bf73
    }

    .btn-success {
      padding: 5px 25px;
      background-color: #21bf73
    }

    .btn-primary button:disabled {
      opacity: 0.5;
      background: #9ACFCC;
      color: #00403C;
      cursor: default;
    }

    @media(max-width:576px) {
      #question {
        width: 100%;
        word-spacing: 2px
      }
    }



    a {
      color: #92badd;
      display: inline-block;
      text-decoration: none;
      font-weight: 400;
    }

    h2 {
      text-align: center;
      font-size: 16px;
      font-weight: 600;
      text-transform: uppercase;
      display: inline-block;
      margin: 40px 8px 10px 8px;
      color: #cccccc;
    }



    /* STRUCTURE */

    .wrapper {
      display: flex;
      align-items: center;
      flex-direction: column;
      justify-content: center;
      width: 100%;
      min-height: 100%;
      padding: 20px;
    }

    #formContent {
      -webkit-border-radius: 10px 10px 10px 10px;
      border-radius: 10px 10px 10px 10px;
      background: #fff;
      padding: 30px;
      width: 90%;
      max-width: 450px;
      position: relative;
      padding: 0px;
      -webkit-box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
      box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
      text-align: center;
    }

    /* TABS */

    h2.inactive {
      color: #cccccc;
    }

    h2.active {
      color: #0d0d0d;
      border-bottom: 2px solid #5fbae9;
    }



    /* FORM TYPOGRAPHY*/

    input[type=button],
    input[type=submit],
    input[type=reset] {
      background-color: #56baed;
      border: none;
      color: white;
      padding: 15px 80px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      text-transform: uppercase;
      font-size: 13px;
      -webkit-box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
      box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
      -webkit-border-radius: 5px 5px 5px 5px;
      border-radius: 5px 5px 5px 5px;
      margin: 5px 20px 40px 20px;
      -webkit-transition: all 0.3s ease-in-out;
      -moz-transition: all 0.3s ease-in-out;
      -ms-transition: all 0.3s ease-in-out;
      -o-transition: all 0.3s ease-in-out;
      transition: all 0.3s ease-in-out;
    }

    input[type=button]:hover,
    input[type=submit]:hover,
    input[type=reset]:hover {
      background-color: #39ace7;
    }

    input[type=button]:active,
    input[type=submit]:active,
    input[type=reset]:active {
      -moz-transform: scale(0.95);
      -webkit-transform: scale(0.95);
      -o-transform: scale(0.95);
      -ms-transform: scale(0.95);
      transform: scale(0.95);
    }

    input[type=text] {
      background-color: #f6f6f6;
      border: none;
      color: #0d0d0d;
      padding: 15px 32px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      margin: 5px;
      width: 85%;
      border: 2px solid #f6f6f6;
      -webkit-transition: all 0.5s ease-in-out;
      -moz-transition: all 0.5s ease-in-out;
      -ms-transition: all 0.5s ease-in-out;
      -o-transition: all 0.5s ease-in-out;
      transition: all 0.5s ease-in-out;
      -webkit-border-radius: 5px 5px 5px 5px;
      border-radius: 5px 5px 5px 5px;
    }

    input[type=text]:focus {
      background-color: #fff;
      border-bottom: 2px solid #5fbae9;
    }

    input[type=text]:placeholder {
      color: #cccccc;
    }



    /* ANIMATIONS */

    /* Simple CSS3 Fade-in-down Animation */
    .fadeInDown {
      -webkit-animation-name: fadeInDown;
      animation-name: fadeInDown;
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }

    @-webkit-keyframes fadeInDown {
      0% {
        opacity: 0;
        -webkit-transform: translate3d(0, -100%, 0);
        transform: translate3d(0, -100%, 0);
      }

      100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
      }
    }

    @keyframes fadeInDown {
      0% {
        opacity: 0;
        -webkit-transform: translate3d(0, -100%, 0);
        transform: translate3d(0, -100%, 0);
      }

      100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
      }
    }

    /* Simple CSS3 Fade-in Animation */
    @-webkit-keyframes fadeIn {
      from {
        opacity: 0;
      }

      to {
        opacity: 1;
      }
    }

    @-moz-keyframes fadeIn {
      from {
        opacity: 0;
      }

      to {
        opacity: 1;
      }
    }

    @keyframes fadeIn {
      from {
        opacity: 0;
      }

      to {
        opacity: 1;
      }
    }

    .fadeIn {
      opacity: 0;
      -webkit-animation: fadeIn ease-in 1;
      -moz-animation: fadeIn ease-in 1;
      animation: fadeIn ease-in 1;

      -webkit-animation-fill-mode: forwards;
      -moz-animation-fill-mode: forwards;
      animation-fill-mode: forwards;

      -webkit-animation-duration: 1s;
      -moz-animation-duration: 1s;
      animation-duration: 1s;
    }

    .fadeIn.first {
      -webkit-animation-delay: 0.4s;
      -moz-animation-delay: 0.4s;
      animation-delay: 0.4s;
    }

    .fadeIn.second {
      -webkit-animation-delay: 0.6s;
      -moz-animation-delay: 0.6s;
      animation-delay: 0.6s;
    }

    .fadeIn.third {
      -webkit-animation-delay: 0.8s;
      -moz-animation-delay: 0.8s;
      animation-delay: 0.8s;
    }

    .fadeIn.fourth {
      -webkit-animation-delay: 1s;
      -moz-animation-delay: 1s;
      animation-delay: 1s;
    }

    *:focus {
      outline: none;
    }

  </style>
</head>

<body>

  <div class="wrapper fadeInDown pt-5">
    <div id="formContent" class="pt-5">
    <p class="h1 text-center fadeIn">Quiz</p>
      <!-- Login Form -->
      <form id='user-form'>
        <input type="text" name='username' id='username' class="fadeIn second" placeholder="Enter username or phone">
        <input type="submit" class="fadeIn fourth" value="Go" id='go-button'>
      </form>
    </div>
  </div>

  <div class="container mt-sm-5 my-1">
    <div id="quiz">
      <div class="float-right"> <button class="btn btn-success" id="submit-button">Submit Answers</button> </div>

      <h1 id="quiz-name"></h1>
      <div class="d-flex align-items-center pt-3">
        <div id="prev"> <button class="btn btn-primary" id="prev-question-button">Previous</button> </div>
        <div class="ml-auto"> <button class="btn btn-success" id="next-question-button">Next</button> </div>
      </div>

      <div class="text-center" id="quiz-results">
        <p id="quiz-results-message"></p>
        <p id="quiz-results-score"></p>
      </div>
    </div>
  </div>
  <!-- jQuery -->
  <script src="<?= ASSETS_URL ?>js/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= ASSETS_URL ?>js/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="<?= ASSETS_URL ?>js/lib/RestAdapter.js"></script>
  <script src="<?= ASSETS_URL ?>js/lib/StringUtil.js"></script>
  <script src="<?= ASSETS_URL ?>js/adminLTE/custom.js"></script>
  <script src="<?= ASSETS_URL ?>js/pages/quiz.js"></script>

</body>

</html>