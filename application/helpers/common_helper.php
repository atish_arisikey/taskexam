<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

function isLoggedIn() 
{
    $ci=& get_instance();
    $user_id = $ci->session->userdata('aa_user_id');
    if($user_id) 
        return true;
    else 
        redirect('login');
}

function checkDuplicateEntry($tableName,$select,$whereClause) 
{
	$ci=& get_instance();
	$id = $ci->db->select($select)->get_where($tableName, $whereClause)->row_array();
	return $id[$select];
}                 

function EncryptStr($password,$key)
{

	$encrypted_string=openssl_encrypt($password,"AES-128-ECB",$key);
	return  $encrypted_string;
}

function DecryptStr($password,$key)
{

	$decrypted_string=openssl_decrypt($password,"AES-128-ECB",$key);
	return  $decrypted_string;
}

// function generate_img($imgStr,$path)
// {
// 	$img = str_replace('data:image/png;base64,', '', $imgStr);
// 	$img = str_replace(' ', '+', $img);
// 	$data = base64_decode($img);
// 	$file = $path . uniqid() . '.png';
// 	file_put_contents($file, $data);
// 	return $file;
// }

function generate_img($img_arr,$path,$old_img)
{
	$img_path_arr = array();
	if($img_arr!='')
	{
		$img_arr = explode(',', $img_arr);
		foreach ($img_arr as $key => $value) {
			$img = $value;
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$file = $path .uniqid() . '.jpg';
			file_put_contents($file, $data);
			array_push($img_path_arr,base_url().$file);
			}
	}
	else if($old_img!='')
	{
		array_push($img_path_arr,$old_img);	
	}
	else
	{
		array_push($img_path_arr,'assets/media/logo/1.png');		
	}
	$image_path_str = implode(',',$img_path_arr);
	return $image_path_str;
}

function generate_multiple_img($img_arr,$path,$old_img)
{
	$img_path_arr = array();
	
	if($img_arr != '')
	{
		$img_arr = explode(',', $img_arr);
		//$img_path_arr = array();
		foreach ($img_arr as $key => $value) {
			$img = $value;
			//$img = str_replace('data:image/png;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$file = $path . uniqid() . '.jpg';
			file_put_contents($file, $data);
			array_push($img_path_arr,$file);
			}

		//$image_path_str = implode(',',$img_path_arr);
	}
		
	
	if($old_img != '')
	{
		array_push($img_path_arr,$old_img);	
	}
	//print_r($img_path_arr); die;
	$image_path_str = implode(',',$img_path_arr);
	return $image_path_str;
}

function set_pagination_helper($url,$total_record,$per_page,$page,$suffix,$uri_segment = 4, $showing_total_record, $flg_pagination_info = true)
{
	$CI =& get_instance();
	
	$CI->load->library('pagination');

	$config['base_url'] = $url;
	$config['total_rows'] = $total_record;
	$config['per_page'] = $per_page;
	$config['suffix'] = $suffix;
	$config['uri_segment']  = $uri_segment;
	$config['use_page_numbers'] = TRUE;
	$config['full_tag_open'] = '<ul class="pagination" style="margin-top:0px;">';
	$config['full_tag_close'] = '</ul>';
	$config['first_tag_open'] = '<li class="paginate_button first">';
	$config['first_tag_close'] = '</li>';
	$config['prev_tag_open'] = '<li class="paginate_button previous">';
	$config['prev_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
	$config['cur_tag_close'] = '</a></li>';
	$config['num_tag_open'] = '<li class="paginate_button">';
	$config['num_tag_close'] = '</li>';
	$config['next_tag_open'] = '<li class="paginate_button next">';
	$config['next_tag_close'] = '</li>';
	$config['last_tag_open'] = '<li class="paginate_button last">';
	$config['last_tag_close'] = '</li>';
	$config['first_link'] = 'First';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Previous';
	$config['last_link'] = 'Last';
	
	$config['first_url'] = $config['base_url'] . $config['suffix'];
	
	$showing_have_total_record = $total_record;
	
	$showing_start_record = ($page==0) ? 1 : $page + 1;
	
	$showing_end_record = ($page==0) ? $page + $showing_total_record : $showing_start_record + ($showing_total_record -1);
	
	$CI->pagination->initialize($config);

	$pagination = $CI->pagination->create_links();
	
	if($flg_pagination_info == true)
	{
		$pagination_html = '<div class="col-sm-5">
								<div class="dataTables_info" id="example_info" role="status" aria-live="polite">
									Showing '.$showing_start_record.' to '.$showing_end_record.' of '.$showing_have_total_record.' entries
								</div>
							</div>
							<div class="col-sm-7">
								<div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
									'.$pagination.'
								</div>
							</div>';
	}
	else
	{
		$pagination_html = '<div class="col-sm-12 text-center">
								<div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
									'.$pagination.'
								</div>
							</div>';
	}
					
	return $pagination_html;				
}

function convert_url($url_string)
{
	$url_string = strtolower($url_string);
	
	$replace_to_array = array("!","@", "#", "$", "%", "^", "&", "*", "+", "=", "?", "~", "™", "®");
	
	$url_string = str_replace($replace_to_array, "", $url_string);
	
	$replace_to_array = array(" ","'","!","@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", '"', ":", ";", "<", ">", ",", ".", "?", "/", "\/", "|", "{", "}", "[", "]", "~", "`");
	
	$url_string = str_replace($replace_to_array, "", $url_string);
	
	$replace_to_array = array("Ç","ç","Ğ","ğ", "İ", "ı", "Ö", "ö", "Ş", "ş", "Ü", "ü");
	$replace_with_array = array("c","c","g","g", "i", "i", "o", "o", "s", "s", "u", "u");
	
	$result = str_replace($replace_to_array, $replace_with_array, $url_string);
	
	return $result;
}

function clean_string($data)
{
	$data = array_map(function($value) { return trim($value); }, $data);
	return $data;
}

function format_currency($amount = '',$currency = '')
    {
		$cur_symbol = '';
        $pos = '';
        $dec = 2;
        $dec_sep = '.';
        $thou_sep = '';
        $cur_before = $cur_symbol.''; $cur_after = '';
        if ($pos == 'before') { $cur_before = $cur_symbol.''; $cur_after = ''; }
        if ($pos == 'after') { $cur_before = ''; $cur_after = ''.$cur_symbol; }

        return $cur_before.number_format($amount,$dec,$dec_sep,$thou_sep).$cur_after;
    }
	
function get_db_backup()
{	
	$CI =& get_instance();
	
	$fileName = strtolower('db_demo');
	$fileName = str_replace(' ','_',$fileName);
	
	$fileName = $fileName.'.zip';

	// Load the DB utility class
	$CI->load->dbutil();

	// Backup your entire database and assign it to a variable
	$backup =& $CI->dbutil->backup();

	// Load the file helper and write the file to your server
	$CI->load->helper('file');
	write_file(getcwd().'/media/db_backup/'.$fileName, $backup);

}

function convert_number_to_words($number) {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' Rupees ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Sixteen',
        17                  => 'Seventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Fourty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
	
    if (null !== $fraction && is_numeric($fraction) && $fraction!='00') {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

function convert_name($url_string)
{
	$url_string = strtolower($url_string);
	
	$replace_to_array = array(" ","'","!","@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", '"', ":", ";", "<", ">", ",", ".", "?", "/", "\/", "|", "{", "}", "[", "]", "~", "`");
	
	return $result = str_replace($replace_to_array, "-", $url_string);
}

function get_user_image($user_id)
{
	$CI =& get_instance();
	
	$CI->db->select('st_profile_picture');
	$CI->db->from('tbl_users');
	$CI->db->where("in_user_id = '".$user_id."' AND flg_is_active = 1 AND flg_is_delete = 0");
	$query = $CI->db->get();
	$row_data = $query->row();
	if(!empty($row_data->st_profile_picture))
	{
		return base_url().'media/user_image/'.$row_data->st_profile_picture;
	}
	else  { return null;}
}

function system_log($post_data, $action)
{
	$CI =& get_instance();
	$insert_data['in_user_id'] = $CI->session->userdata('user_id');
	$insert_data['st_post'] = $post_data;
	$insert_data['st_updated_feild'] = $action;
	$insert_data['dt_date_time'] = date('Y-m-d H:i');
	$CI->db->insert('tbl_system_log',$insert_data);
}

function admin_access_roles()
{
	$CI =& get_instance();
	$access_role = array();
	
	$access_role[1] = array('flg_user_mangement'=>1,
							'flg_customer_mangement'=>1,
							'flg_employee'=>1,
							'flg_new_employee'=>1,
							'flg_edit_employee'=>1,
							'flg_delete_employee'=>1,
							'flg_customer'=>1,
							'flg_new_customer'=>1,
							'flg_edit_customer'=>1,
							'flg_delete_customer'=>1,
							'flg_company_settings'=>1,
							);
	
	$access_role[2] = array('flg_user_mangement'=>1,
							'flg_customer_mangement'=>1,
							'flg_employee'=>1,
							'flg_new_employee'=>1,
							'flg_edit_employee'=>1,
							'flg_delete_employee'=>1,
							'flg_customer'=>1,
							'flg_new_customer'=>1,
							'flg_edit_customer'=>1,
							'flg_delete_customer'=>1,
							'flg_company_settings'=>1,
							);						
	
	$access_role[3] = array('flg_user_mangement'=>1,
							'flg_customer_mangement'=>1,
							'flg_employee'=>1,
							'flg_new_employee'=>1,
							'flg_edit_employee'=>1,
							'flg_delete_employee'=>1,
							'flg_customer'=>1,
							'flg_new_customer'=>1,
							'flg_edit_customer'=>1,
							'flg_delete_customer'=>1,
							'flg_company_settings'=>1,
							);	
	
	return $access_role;							
}

function get_role_access()
{
	$CI =& get_instance();
   
    $role_id = $CI->session->userdata('role_id');
	$access_role = admin_access_roles();
	if(!empty($access_role[$role_id]))
	{
		foreach($access_role[$role_id] as $key=>$val):
			
			$CI->config->set_item($key, $val);
			
		endforeach;
	}
}

function privilege($item_name)
{
	
	$CI =& get_instance();
	$role_id = $CI->session->userdata('role_id');
	
	$item_value = $CI->config->item($item_name);
	
	if(!empty($role_id) && $item_value==0){ return 'style="display:none"'; }
	else { return null; }
}

function restrict_privileges($item_name='')
{
	if(!empty($item_name))
	{
		$CI =& get_instance();
		
		$role_id = $CI->session->userdata('role_id');
		
		$function_item_value = $CI->config->item($item_name);

		if(!empty($role_id) && $function_item_value==0) redirect('admin/dashboard_c');
	}
}

function get_settings()
{
	$CI =& get_instance();
	
	$CI->load->model('setting_m');
	
	$where_cond = "in_id = 1";
	$select_cond = "*";
	$result = $CI->setting_m->select_records($where_cond,$select_cond,'','','','');
	$invoice_data = $result[0];
	return $invoice_data;
}