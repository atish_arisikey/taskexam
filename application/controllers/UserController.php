<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        isLoggedIn();
        $data['page_title'] = 'List View | Website Title';
        $data['page'] = 'user_view';
        $this->load->view('include/template', $data);
    }

    public function getAllData()
    {
        isLoggedIn();
        $response = [
            'status' => 1,
            'message' => 'Ooh! Somthing happened terrible.',
            'data' => '',
        ];

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method === "GET") {
            $page = $_GET['page'];
            $search = $_GET['search'];
            $filterType = '';
            $fromDate = '';
            $toDate = '';
            $sortBy = '';
            $sortType = 'desc';

            if (isset($_GET['filter_type'])) {
                $filterType = $_GET['filter_type'];
            }

            if (isset($_GET['from_date'])) {
                $fromDate = $_GET['from_date'];
            }

            if (isset($_GET['to_date'])) {
                $toDate = $_GET['to_date'];
            }

            if (isset($_GET['sort_by'])) {
                $sortBy = $_GET['sort_by'];
            }

            if (isset($_GET['sort_type'])) {
                $sortType = $_GET['sort_type'];
            }

            $rowsCount = $this->UserModel->getListCount($search, $filterType, $fromDate, $toDate);

            $data = $this->UserModel->getAllData($page, $search, $filterType, $fromDate, $toDate, $sortBy, $sortType);

            if ($data) {
                $response = [
                    'status' => 0,
                    'message' => 'Success',
                    'data' => $data,
                    'rowsCount' => $rowsCount
                ];
            } else {
                $response = [
                    'status' => 1,
                    'message' => 'Record not found',
                    'data' => null,
                    'rowsCount' => null
                ];
            }
        } else {
            $response = [
                'status' => 1,
                'message' =>  'Request method cannot be accepted.',
                'data' => null,
            ];
        }

        echo json_encode($response);
    }

    public function insertData()
    {
        isLoggedIn();
        $response = [
            'status' => 1,
            'message' => 'Ooh! Somthing happened terrible.',
            'data' => '',
        ];

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method === "POST") {

            $id = "";
            if (isset($_POST['user_id'])) {
                $id = $_POST['user_id'];
            }

            $fullName = "";
            if (isset($_POST['full_name'])) {
                $fullName = $_POST['full_name'];
            }

            $mobile = "";
            if (isset($_POST['mobile'])) {
                $mobile = $_POST['mobile'];
            }

            $emailId = "";
            if (isset($_POST['email_id'])) {
                $emailId = $_POST['email_id'];
            }

            $dateOfBirth = "";
            if (isset($_POST['date_of_birth'])) {
                $dateOfBirth = $_POST['date_of_birth'];
            }

            $isDublicate = checkDuplicateEntry('Users', 'UserId', ['Contact' => $mobile]);

            if ($id === $isDublicate || empty($isDublicate)) {
                if (empty($id)) {
                    $employeeId = $this->UserModel->insertData($fullName, $mobile, $emailId, $dateOfBirth);
                    $response = [
                        'status' => 0,
                        'message' => 'Added successfully!',
                        'data' => ['Name' => $fullName, 'Mobile' => $mobile, 'Email' => $emailId]
                    ];
                } else {
                    $updated = $this->UserModel->updateData($id, $fullName, $mobile, $emailId, $dateOfBirth);
                    $response = [
                        'status' => 0,
                        'message' => 'Updated successfully!',
                        'data' => $updated
                    ];
                }
            } else {
                $response = array(
                    'status' => 1,
                    'message' => $mobile . ' already exists',
                    'inserted' => false,
                );
            }
        } else {
            $response = [
                'status' => 1,
                'message' =>  'Request method cannot be accepted.',
                'data' => null,
            ];
        }

        echo json_encode($response);
    }

    public function deleteData()
    {
        isLoggedIn();
        $response = [
            'status' => 1,
            'message' => 'Ooh! Somthing happened terrible.',
            'data' => '',
        ];

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method === "POST") {

            $id = "";
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
            }

            $deleted = $this->UserModel->deleteData($id);

            if ($deleted) {
                $response = [
                    'status' => 0,
                    'message' =>  'Deleted successfully.',
                    'data' => $deleted,
                ];
            } else {
                $response = [
                    'status' => 1,
                    'message' =>  'Not Deleted, please try again leter.',
                    'data' => null,
                ];
            }
        } else {
            $response = [
                'status' => 1,
                'message' =>  'Request method cannot be accepted.',
                'data' => null,
            ];
        }

        echo json_encode($response);
    }

    public function getUserDetails()
    {
        $reponse = [
            'status' => 1,
            'message' => 'Ooh! Somthing happened terrible.',
            'data' => '',
        ];

        $username = htmlentities(strip_tags(trim($_POST['username'])));

        $userInformation = $this->UserModel->getUserDetails($username);
        if (isset($userInformation)) {
            $reponse = [
                'status' => 0,
                'message' => 'success',
                'data' => $userInformation,
            ];
        } else {
            $reponse = [
                'status' => 1,
                'message' => 'You have entered incorrect username or phone.',
                'data' => null,
            ];
        }
        echo json_encode($reponse);
    }

    public function insertScore()
    { 
            $response = [
                'status' => 1,
                'message' => 'Ooh! Somthing happened terrible.',
                'data' => '',
            ];

            $method = $_SERVER['REQUEST_METHOD'];
            if ($method === "POST") {

                $id = "";
                if (isset($_POST['user_id'])) {
                    $id = $_POST['user_id'];
                }

                $score = 0;
                if (isset($_POST['score'])) {
                    $score = $_POST['score'];
                }


                if (!empty($id)) {
                    $employeeId = $this->UserModel->insertScore($id, $score);
                    $response = [
                        'status' => 0,
                        'message' => 'Added successfully!',
                        'data' => $score
                    ];
                }
            } else {
                $response = [
                    'status' => 1,
                    'message' =>  'Request method cannot be accepted.',
                    'data' => null,
                ];
            }

            echo json_encode($response);
        }
    
}
