let restAdapter = new RestAdapter();
let stringUtil = new StringUtil();
let userData = [];

var Quiz = function(quizName) {

    this.quizName = quizName;

    this.questions = [];
}

Quiz.prototype.addQuestion = function(question) {
    var indexToAddQuestion = Math.floor(Math.random() * this.questions.length);
    this.questions.splice(indexToAddQuestion, 0, question);
}

Quiz.prototype.render = function(container) {
    var self = this;

    $('#quiz-results').hide();

    $('#quiz-name').text(this.quizName);

    var questionContainer = $('<div>').attr('id', 'question').attr('class', 'ml-sm-5 pl-sm-5 pt-2').insertAfter('#quiz-name'); //

    function change_question() {
        self.questions[currentQuestionIndex].render(questionContainer);
        $('#prev-question-button').prop('disabled', currentQuestionIndex === 0);
        $('#next-question-button').prop('disabled', currentQuestionIndex === self.questions.length - 1);

        var allQuestionsAnswered = true;
        for (var i = 0; i < self.questions.length; i++) {
            if (self.questions[i].userChoiceIndex === null) {
                allQuestionsAnswered = false;
                break;
            }
        }
        $('#submit-button').prop('disabled', !allQuestionsAnswered);
    }

    var currentQuestionIndex = 0;
    change_question();

    $('#prev-question-button').click(function() {
        if (currentQuestionIndex > 0) {
            currentQuestionIndex--;
            change_question();
        }
    });

    $('#next-question-button').click(function() {
        if (currentQuestionIndex < self.questions.length - 1) {
            currentQuestionIndex++;
            change_question();
        }
    });

    $('#submit-button').click(function() {
        var score = 0;
        for (var i = 0; i < self.questions.length; i++) {
            if (self.questions[i].userChoiceIndex === self.questions[i].correctChoiceIndex) {
                score++;
            }
        }

        var percentage = score / self.questions.length;
        console.log(percentage);
        var message;
        if (percentage === 1) {
            message = 'Great job!'
        } else if (percentage >= .75) {
            message = 'You did alright.'
        } else if (percentage >= .5) {
            message = 'Better luck next time.'
        } else {
            message = 'Maybe you should try a little harder.'
        }
        $('#quiz-results-message').text(message);
        $('#quiz-results-score').html('You got <b>' + score + '/' + self.questions.length + '</b> questions correct.');
        $('#quiz-results').slideDown();
        $('#submit-button').slideUp();
        $('#next-question-button').slideUp();
        $('#prev-question-button').slideUp();
        insertScore(score);
    });

    questionContainer.bind('user-select-change', function() {
        var allQuestionsAnswered = true;
        for (var i = 0; i < self.questions.length; i++) {
            if (self.questions[i].userChoiceIndex === null) {
                allQuestionsAnswered = false;
                break;
            }
        }
        $('#submit-button').prop('disabled', !allQuestionsAnswered);
    });
}

var Question = function(questionString, correctChoice, wrongChoices) {
    this.questionString = questionString;
    this.choices = [];
    this.userChoiceIndex = null;

    this.correctChoiceIndex = Math.floor(Math.random(0, wrongChoices.length + 1));

    var numberOfChoices = wrongChoices.length + 1;
    for (var i = 0; i < numberOfChoices; i++) {
        if (i === this.correctChoiceIndex) {
            this.choices[i] = correctChoice;
        } else {
            var wrongChoiceIndex = Math.floor(Math.random(0, wrongChoices.length));
            this.choices[i] = wrongChoices[wrongChoiceIndex];
            wrongChoices.splice(wrongChoiceIndex, 1);
        }
    }
}

Question.prototype.render = function(container) {
    var self = this;

    var questionStringHeading;
    if (container.children('div').length === 0) {
        questionStringHeading = $('<div>').attr('class', 'py-2 h5').appendTo(container);
    } else {
        questionStringHeading = container.children('div').first();
    }
    questionStringHeading.text(this.questionString);

    if (container.children('input[type=radio]').length > 0) {
        container.children('input[type=radio]').each(function() {
            var radioButtonId = $(this).attr('id');
            $(this).remove();
            container.children('label[for=' + radioButtonId + ']').remove();
        });
    }
    for (var i = 0; i < this.choices.length; i++) {
        var choice_radio_button = $('<input>')
            .attr('id', 'choices-' + i)
            .attr('type', 'radio')
            .attr('name', 'choices')
            .attr('value', 'choices-' + i)
            .attr('checked', i === this.userChoiceIndex)
            .appendTo(container);

        var choice_label = $('<label>')
            .text(this.choices[i])
            .attr('for', 'choices-' + i)
            .attr('class', 'options')
            .appendTo(container);
    }

    $('input[name=choices]').change(function(index) {
        var selectedRadioButtonValue = $('input[name=choices]:checked').val();

        self.userChoiceIndex = parseInt(selectedRadioButtonValue.substr(selectedRadioButtonValue.length - 1, 1));

        container.trigger('user-select-change');
    });
}

$(document).ready(function() {
    $('.container').hide();
    $('#go-button').on('click', function(e) {
        e.preventDefault();
        getUserDetails();
    });
    // goToQuiz();
});

let userValid = () => {
    let flag = true;
    let userName = stringUtil.getString(stringUtil.strip_html_tags($('#username').val()));
    if (userName === '') {
        alert('Enter username or phone');
        $('#username').focus();
        flag = false;
    }

    return flag;
}

function getUserDetails() {
    if (userValid()) {
        let formData = new FormData($('#user-form')[0]);
        restAdapter.executePOST('get_user_details', formData, function(response) // function_name([api_url],[json_array(field_parameter)],[callback_function])
            {
                jsonObject = JSON.parse(response);
                if (jsonObject.status === 0) {
                    if (jsonObject.data.Score === null) {
                        userData = jsonObject.data;
                        $('.wrapper').remove();
                        goToQuiz();
                    } else {
                        $('.container').show();
                        $('#quiz-results-score').html('You\'r  score is <b>' + jsonObject.data.Score + '</b>.');
                        $('#quiz-results').slideDown();
                        $('#submit-button').slideUp();
                        $('#next-question-button').slideUp();
                        $('#prev-question-button').slideUp();
                        $('.wrapper').remove();
                    }

                } else {
                    alert(jsonObject.message);
                }
            });
    }
}

function goToQuiz() {
    $('.container').show();
    var quiz = new Quiz('MCQ Quiz');
    restAdapter.executeGET('https://opentdb.com/api.php?amount=10', '', function(response) {
        var allQuestions = response.results;

        for (var i = 0; i < allQuestions.length; i++) {
            var question = new Question(allQuestions[i].question, allQuestions[i].correct_answer, allQuestions[i].incorrect_answers);
            quiz.addQuestion(question);
        }
        var quizContainer = $('#quiz');
        quiz.render(quizContainer);
    });
}

function insertScore(score) {
    let formData = new FormData();
    formData.append('user_id', userData.UserId);
    formData.append('score', score);
    restAdapter.executePOST('insert_score', formData, function(response) {
        console.log('Reocrd saved.');
    });
}