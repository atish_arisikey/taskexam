<?php
function htmlEncodeForApp($htmlString)
{
    $htmlString = str_replace("\r", "", $htmlString);
    $htmlString = str_replace("\n\n", "\n", $htmlString);
    $htmlString = str_replace("&quot;", "\"", $htmlString);
    $htmlString = str_replace("&quot;", "\"", $htmlString);
    $htmlString = str_replace("&#39;", "\'", $htmlString);
    $htmlString = str_replace("&amp;", "&", $htmlString);
    $htmlString = str_replace("&lt;", "<", $htmlString);
    $htmlString = str_replace("&gt;", ">", $htmlString);

    return $htmlString;
}

function removeHtmlSpecialChars($text)
{
    $text = trim($text);
    $text =str_replace("&quot;", '"',$text);
    $text =str_replace('\"', '"',$text);
    $text =str_replace("&amp;", "&",$text);
    $text =str_replace("&lt;", "<",$text);
    $text =str_replace("&gt;", ">",$text);
    return $text;
}