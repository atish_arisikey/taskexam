let restAdapter = new RestAdapter();
let stringUtil = new StringUtil();
let pageNumber = 1;
let rowsPerPage = 0;
let searchKeyword = '';
let columnName = '';
let params = '';
let sort = '';
let attribute = '';
let userId = 0;
let fullName = '';
let mobile = '';
let emailId = '';
let dateOfBirth = '';
let userData = [];
// let contentAccess = '';
$(function() {
    search(1);

    $('#filterType').on('change', function() {
        $('#fromDate').val('');
        $('#toDate').val('');
        let filterType = this.value;
        let fromDateDiv = document.querySelectorAll('#fromDateDiv');
        let toDateDiv = document.querySelectorAll('#toDateDiv');

        switch (filterType) {
            case 'date':
                fromDateDiv[0].style.display = 'flex';
                toDateDiv[0].style.display = 'none';
                fromDateDiv[0].children.fromDateLabel.innerHTML = 'Date:';
                break;

            case 'weekly':
                fromDateDiv[0].style.display = 'flex';
                toDateDiv[0].style.display = 'none';
                fromDateDiv[0].children.fromDateLabel.innerHTML = 'Date:';
                break;

            case 'between':
                fromDateDiv[0].style.display = 'flex';
                toDateDiv[0].style.display = 'flex';
                fromDateDiv[0].children.fromDateLabel.innerHTML = 'From:';

                break;

            default:
                fromDateDiv[0].style.display = 'none';
                toDateDiv[0].style.display = 'none';
                fromDateDiv[0].children.fromDateLabel.innerHTML = 'From:';

                break;
        }
    });

    $('.column_sort').on('click', function(e) {
        e.preventDefault();
        columnName = $(this).attr("id");
        sort = $(this).attr("sort");
        var attribute = '';
        if (sort == 'desc') {
            $('#' + columnName).find('i').removeClass("fa-long-arrow-alt-up").addClass("fa-long-arrow-alt-down");
            attribute = 'asc';
        } else {
            $('#' + columnName).find('i').removeClass("fa-long-arrow-alt-down").addClass("fa-long-arrow-alt-up");
            attribute = 'desc';
        }
        $(this).attr("sort", attribute);
        search(pageNumber, searchKeyword);
    });
});

function filter() {
    search(pageNumber);
}

function search(pageNumber, searchKeyword = '') {
    var filterType = $('#filterType').val();
    var fromDate = $('#fromDate').val();
    var toDate = $('#toDate').val();
    pageNumber = pageNumber - 1;
    params = '?page=' + pageNumber + '&search=' + searchKeyword + '&sort_by=' + columnName + '&sort_type=' + sort + '&filter_type=' + filterType + '&from_date=' + fromDate + '&to_date=' + toDate;
    restAdapter.executeGET('get_user_list', params, function(response) {
        onSuccess(response);
    });
}

$("body").on("click", ".Pager .page", function() {
    pageNumber = parseInt($(this).attr('page'));
    search(pageNumber, searchKeyword);
});

function onSuccess(response) {
    var json_object = JSON.parse(response);
    userData = json_object.data;
    if (userData != null || userData != '') {
        $("#tableDetails tbody tr").remove();
        $.each(json_object.data, function(index, value) {
            // let isCheck = (value.Status === '0') ? '' : 'checked';
            let scores = (value.Scores > -1) ? value.Scores : 'Pending';
            let markup = '<tr>' +
                '<td>' + index + '</td>' +
                '<td>' + value.FullName + '</td>' +
                '<td>' + value.EmailId + '</td>' +
                '<td>' + value.Contact + '</td>' +
                '<td>' + value.DateOfBirth + '</td>' +
                '<td>' + scores + '</td>' +
                '<td>' + value.RegistrationDate + '</td>' +
                // '<td>' +
                // '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">' +
                // '<input type="checkbox" class="custom-control-input isActive" ' + isCheck + ' id=\'approv_' + index + '\' value=\'' + index + '\'>' +
                // '<label class="custom-control-label" for=\'approv_' + index + '\'></label>' +
                // '</div>' +
                // '</td>' +
                '<td>' +
                '<span class="info-box-icon text-success mr-2 Update" title=\'Edit\' onclick=edit(' + index + ',event) style="cursor:pointer">' +
                '<i class="far fa-edit"></i>' +
                '</span>' +
                '<span class="info-box-icon text-danger mr-2 Delete" title=\'Delete\' onclick=confirmation(' + index + ',event) style="cursor:pointer">' +
                '<i class="far fa-trash-alt"></i>' +
                '</span>' +
                '</td>' +
                '</tr>';

            $("#tableDetails tbody").append(markup);

        });

        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: pageNumber,
            PageSize: 20,
            RecordCount: json_object.rowsCount
        });
    } else {
        console.log('Record is NULL');
    }
}

function addUser() {
    clearFileds();
    $('#insertModel').modal('show');
    document.querySelector('#inserButton').setAttribute('onclick', 'insertUser()');
}

function confirmation(indexValue, e) {
    e.preventDefault();
    $('#confirmModal').modal('show');
    document.getElementById('modal-btn-yes').setAttribute('onclick', 'Delete(' + indexValue + ')');
}

function Delete(indexValue) {
    $('#confirmModal').modal('hide');
    let formData = new FormData();
    let deleteId = userData[indexValue].UserId;
    formData.append('id', deleteId);

    restAdapter.executePOST('delete_user', formData, function(response) // function_name([api_url],[json_array(field_parameter)],[callback_function])
        {
            json_object = JSON.parse(response);

            if (json_object.status === 0) {
                $('#insertModel').modal('hide');
                search(pageNumber, searchKeyword);
                alert(json_object.message);
            }
            if (json_object.status === 1) {
                alert(json_object.message);
            }
        });
}


function edit(index, e) {
    e.preventDefault();
    clearFileds();
    document.getElementById('modalTitle').innerHTML = 'Update';
    document.querySelector('#inserButton').setAttribute('onclick', 'updateUser()');
    userId = stringUtil.getString(userData[index].UserId);

    $('#insertModel').modal('show');
    $('#fullName').val(stringUtil.getString(userData[index].FullName));
    $('#mobile').val(stringUtil.getString(userData[index].Contact));
    $('#emailId').val(stringUtil.getString(userData[index].EmailId));
    $('#dateOfBirth').val(stringUtil.getString(userData[index].DateOfBirth));

}

function validation() {
    let phoneRegEx = /^\d{10}$/;
    fullName = stringUtil.getString($('#fullName').val());
    mobile = stringUtil.getString($('#mobile').val());
    emailId = stringUtil.getString($('#emailId').val());
    dateOfBirth = stringUtil.getString($('#dateOfBirth').val());

    if (fullName === '') {
        alert('Enter full name.');
        $('#fullName').focus();
        return false;
    } else if (phoneRegEx.test(mobile) === false) {
        alert('Enter valid mobile number.');
        $('#mobile').focus();
        return false;
    } else if (emailId === '') {
        alert('Enter email ID.');
        $('#emailId').focus();
        return false;
    } else if (dateOfBirth === '') {
        alert('Enter date of birth.');
        $('#dateOfBirth').focus();
        return false;
    }
    return true;
}

function insertUser() {
    if (validation()) {
        var formData = new FormData();

        formData.append('full_name', fullName);
        formData.append('mobile', mobile);
        formData.append('email_id', emailId);
        formData.append('date_of_birth', dateOfBirth);

        restAdapter.executePOST('insert_user', formData, function(response) {
            var json_object = JSON.parse(response);
            if (json_object.status === 0) {
                $('#insertModel').modal('hide');
                search(pageNumber, searchKeyword);
                alert(json_object.message);
                clearFileds();
            }
            if (json_object.status === 1) {
                alert(json_object.message);
            }
        });
    }

};


function updateUser() {
    if (validation()) {
        var formData = new FormData();

        formData.append('full_name', fullName);
        formData.append('mobile', mobile);
        formData.append('email_id', emailId);
        formData.append('date_of_birth', dateOfBirth);
        formData.append('user_id', userId);

        restAdapter.executePOST('update_user', formData, function(response) {

            var json_object = JSON.parse(response);
            if (json_object.status === 0) {
                document.getElementById('modalTitle').innerHTML = 'Add';
                $('#insertModel').modal('hide');
                search(pageNumber, searchKeyword);
                alert(json_object.message);
                clearFileds();
            }
            if (json_object.status === 1) {
                alert(json_object.message);
            }

        });
    }

};

function clearFileds() {
    $('#fromDate').val('');
    $('#toDate').val('');
    $('#fullName').val('');
    $('#mobile').val('');
    $('#emailId').val('');
    $('#dateOfBirth').val('');
}