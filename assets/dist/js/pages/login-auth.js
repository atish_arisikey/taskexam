let restAdapter = new RestAdapter();
let stringUtil = new StringUtil();

(function() {
    // showErrorMessage('hi');
    // alert('bye');
    // showSuccessMessage('tata');
})();

let userValid = () => {
    let flag = true;
    let userName = stringUtil.getString(stringUtil.strip_html_tags($('#username').val()));
    if (userName === '') {
        alert('Enter username or phone');
        $('#username').focus();
        flag = false;
    }

    return flag;
}

function loginValid() {
    let flag = true;
    let password = stringUtil.getString(stringUtil.strip_html_tags($('#password').val()));

    if (password === '') {
        alert('Enter password');
        $('#password').focus();
        flag = false;
    }

    return flag;
}

function setPasswordValidation() {
    let password = stringUtil.getString(stringUtil.strip_html_tags($('#password').val()));
    let confirm_password = stringUtil.getString(stringUtil.strip_html_tags($('#confirm_password').val()));
    let otp = stringUtil.getString(stringUtil.strip_html_tags($('#otp').val()));

    if (password == '') {
        alert('Enter password');
        $('#password').focus();
        return false;
    } else if (confirm_password === '') {
        alert('Enter confirm password');
        $('#confirm_password').focus();
        return false;
    } else if (otp === '') {
        alert('Enter otp');
        $('#otp').focus();
        return false;
    } else if (password != confirm_password) {
        alert('Confirm password does not match');
        $('#confirm_password').focus();
        return false;
    }

    return true;
}

let loginNextButton = () => {
    if (userValid()) {
        // debugger;
        let formData = new FormData($('#loingFrm')[0]);
        let username = stringUtil.getString(stringUtil.strip_html_tags(formData.get('username')));
        formData.append('username', username);

        restAdapter.executePOST('check_login', formData, function(resonse) {
            let object = JSON.parse(resonse);
            // console.log(resonse);
            if (object.flag == 0) {

                $('#divPwd').css('display', 'block');
                $("#username").attr("readonly", "readonly");
                $("#loginFormNextButton").text("Sign in");
                $("#loginFormNextButton").attr("onclick", "loginFormButton()");
                $(".underlineHover").attr("onclick", "forgetPassword()");

                // $('.login-box-msg').html('Welcome');

            } else if (object.flag == 2) {
                let otp = generateOtp(object.id);
                $('#divPwd').css('display', 'block');
                $('#divConfirmPassword').css('display', 'block');
                $('#divOTP').css('display', 'block');


                $("#username").attr("readonly", "readonly");
                $("#loginFormNextButton").text("Submit");
                $("#loginFormNextButton").attr("onclick", "setPasswordButton(" + object.id + ")");
                $('.login-box-msg').html('Set Password');
                $(".underlineHover").attr("onclick", "generateOtp(" + object.id + ")");
                $(".underlineHover").html("Resend OTP");

            } else {
                // alert('Username or phone doe\'s not exist.');
                alert('Username or phone does not exist.');

            }
        });

    }
}

function loginFormButton() {

    if (loginValid()) {
        let json_data = new FormData($('#loingFrm')[0]);
        restAdapter.executePOST('login_authentication', json_data, function(resonse) {

            let object = JSON.parse(resonse);

            if (object.flag == 0) {
                // console.log('data : ', object.data);
                // let userId = object.data.UserId;
                sessionStorage.setItem('userId', '1');

                window.location.href = 'dashboard';
            } else {
                alert(object.message);
            }

        });
    }

}

function setPasswordButton(id) {

    if (setPasswordValidation()) {
        let json_data = new FormData();
        json_data.append('password', $('#password').val());
        json_data.append('otp', $('#otp').val());
        json_data.append('user_id', id);

        restAdapter.executePOST(_base_url + 'set_password', json_data, function(resonse) {

            let object = JSON.parse(resonse);

            if (object.OtpVerified == 1 && object.PasswordSet == true) {
                $('#divPwd').val('');
                $('#divConfirmPassword').css('display', 'none');
                $('#divOTP').css('display', 'none');

                $("#loginFormNextButton").text("Sign in");
                $("#loginFormNextButton").attr("onclick", "loginFormButton()");
                $(".underlineHover").attr("onclick", "forgetPassword()");

                $("#username").attr("readonly", "readonly");

                $('.login-box-msg').html('Set Password');
                $(".underlineHover").html("Forgot Password?");
                alert('Congratulation! New password has been set, Please login');
                generateOtp(id);

            } else {
                //alert(object.message);
                alert('Enter valid OTP');
                //alert('success','fas fa-thumbs-up','Enter password');

            }

        });
    }

}

function generateOtp(id) {
    restAdapter.executeGET(_base_url + 'get_otp', '?user_id=' + id, function(resonse) {

        let object = JSON.parse(resonse);

        if (object.status == 200) {
            return object.Otp;
        }

    });

}