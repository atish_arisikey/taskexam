/*
*

@authour: Atish Chandole, on 12th March 2020

*
*/

class RestAdapter {
    constructor() //Empty Constructor
    {
    }
    executeWithoutFormDataPOST(url, fieldParams, callback) // function_name([api_url],[json_array(field_parameter)],[callback_function])
    {
        $.ajax({
            url: url,
            type: 'POST',
            data: fieldParams,
            success: function (resp) {
                clearTimeout(showTimeout);
                $("#loader_wrapper").css('display', 'none');
                callback(resp);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                clearTimeout(showTimeout);
                $("#loader_wrapper").css('display', 'none');
                callback(errorThrown);
            }
        });
    }

    executePOST(url, fieldParams, callback) // function_name([api_url],[json_array(field_parameter)],[callback_function])
    {
        var showTimeout = setTimeout(function () {
            $("#loader_wrapper").css('display', 'block');
        }, 1000);
        $.ajax({
            url: url,
            type: 'POST',
            data: fieldParams,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (resp) {
                clearTimeout(showTimeout);
                $("#loader_wrapper").css('display', 'none');
                callback(resp);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                clearTimeout(showTimeout);
                $("#loader_wrapper").css('display', 'none');
                callback(errorThrown);
            }
        });
    }

    executeGET(url, queryParams, callback) // function_name([api_url],[json_array9query_parameter0],[callback_function]) ***Under construction
    {

        var showTimeout = setTimeout(function () {
            $("#loader_wrapper").css('display', 'block');
        }, 2000);
        $.ajax({
            url: url + queryParams,
            type: 'GET',
            success: function (resp) {
                clearTimeout(showTimeout);
                $("#loader_wrapper").css('display', 'none');
                callback(resp);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                clearTimeout(showTimeout);
                $("#loader_wrapper").css('display', 'none');
                callback(errorThrown);
            }
        });
    }
}


