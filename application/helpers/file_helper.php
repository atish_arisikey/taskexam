<?php

const IMAGE_BASE_URL = AA_BASE_URL.'uploads/';

function addMedia($file, $userId)
{
    $baseUrl = IMAGE_BASE_URL;
    
    date_default_timezone_set('Asia/Calcutta');
    $todaysDate = date("Y-m-d H:i:s");
    
    $destination_path = getcwd() . DIRECTORY_SEPARATOR;
    
    $destination_path = $destination_path . "uploads/" . $userId . "/";
    
    if (! file_exists($destination_path))
        mkdir($destination_path);
    
    $target_path = $destination_path . basename($file["name"]);
    
    $target_file;
    $filename;
    
    $filename = basename($file["name"]);
    
    date_default_timezone_set('Asia/Calcutta');
    $insertMediaDate = date("Y-m-d H:i:s");
    move_uploaded_file($file['tmp_name'], $target_path);
    
    $filename = $baseUrl . $userId . "/" . $filename;
    try {
        if (! is_dir($target_path) && ! file_exists($target_path)) {
            mkdir($target_path);
        }
        if (! file_exists($target_path))
            move_uploaded_file($file, $target_path);
    } catch (Exception $e) {}
    
    return $filename;
}

function deleteFile($fileUrl){
    if(file_exists($fileUrl))
    {
        $fileUrl = str_replace(IMAGE_BASE_URL, "uploads/", $fileUrl);
        unlink($fileUrl);
    } 
}

?>