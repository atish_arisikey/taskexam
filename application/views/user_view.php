<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-3">
          <h1>User List</h1>
        </div>
        <div class="col-sm-5">
          <div class='row form-inline'>
            <div class='col-sm-4'>
              <select type="text" id="filterType" class="form-control float-sm-right">
                <option value='all'>--select--</option>
                <option value='all'>All</option>
                <option value='date'>Date</option>
                <option value='today'>Today</option>
                <option value='weekly'>Weekly</option>
                <option value='between'>Between Date</option>
              </select>
            </div>
            <div class='col-sm-4' id='fromDateDiv' style="display: none;">
              <label for="fromDate" id='fromDateLabel' class="float-sm-left">From:</label>
              <input type="date" id='fromDate' class="form-control float-sm-left">
            </div>
            <div class='col-sm-4' id='toDateDiv' style="display: none;">
              <label for="toDate" id='fromDateLabel' class="float-sm-left">To:</label>
              <input type="date" id='toDate' class="form-control float-sm-left">
            </div>
          </div>
        </div>
        <div class="col-sm-1">
          <a href="javascript:void(0);" class="btn btn-dark float-sm-left" onclick=filter()>
            <i class="fas fa-filter"></i>
          </a>
        </div>
        <div class="col-sm-3">
          <a href="javascript:void(0);" class="btn btn-dark Insert float-sm-right" onclick=addUser()>
            <i class="fas fa-user-plus"></i>&nbsp;&nbsp;Add User
          </a>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>



  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <!-- /.row -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Users Details</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 200px;">
                  <input type="text" onkeyup='search(1,this.value)' name="table_search" class="form-control float-right" placeholder="Search">
                  <div class="input-group-append">
                    <button type="submit" onclick=search(1,this.value) class="btn btn-default"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="min-height: 20vh;">
              <table class="table table-head-fixed table-bordered text-nowrap" id='tableDetails'>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>DOB</th>
                    <th><a class="column_sort" id="Score" sort="asc" href="javascript:void(0);" style="color: #000"><i class="fas fa-long-arrow-alt-down"></i>Scores</a></th>
                    <th>Registration On</th>
                    <!-- <th>Active</th> -->
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- Pagination -->
        <div class="card-footer Pager clearfix">
        </div>
        <!-- /.Pagination -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->

  <!-- ------Add Model-------->
  <div class="modal fade" id="insertModel">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><span id='modalTitle'>Add</span> User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <!-- Profile Image -->
              <div class="card card-warning card-outline">
                <div class="card-body box-profile">
                  <ul class="list-group list-group-unbordered mb-3">

                    <li class="list-group-item">
                      <div class="form-group row mb-0">
                        <label class="col-sm-3 col-form-label">Full Name</label>
                        <div class="col-md-8">
                          <input type="text" id="fullName" class="form-control" value="" placeholder="Enter full name">
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="form-group row mb-0">
                        <label class="col-sm-3 col-form-label">Mobile</label>
                        <div class="col-md-8">
                          <input type="number" maxlength="10" min='0' max='9999999999' id="mobile" class="form-control" value="" placeholder="Enter mobile number">
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="form-group row mb-0">
                        <label class="col-sm-3 col-form-label">Email ID</label>
                        <div class="col-md-8">
                          <input type="email" id="emailId" class="form-control" value="" placeholder="Enter email ID" required>
                        </div>
                      </div>
                    </li>

                    <li class="list-group-item">
                      <div class="form-group row mb-0">
                        <label class="col-sm-3 col-form-label">Date Of Birth</label>
                        <div class="col-md-8">
                          <input type="date" id="dateOfBirth" class="form-control" value="" placeholder="Enter date of birth">
                        </div>
                      </div>
                    </li>
                  </ul>
                  <!--a href="#" class="btn btn-primary btn-block"><b>Follow</b></a-->
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" onclick="insertUser()" id='inserButton' class="btn btn-dark" style="background-color: #000000;border-color: #3a3939;border-radius: 22px;min-width: 200px;height: 45px;margin-top: -10px;display: block;margin: 0 auto;"><i class="fas fa-paper-plane mr-2"></i>Submit</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.add modal -->

  <!-- ------Confirm Model-------->
  <div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-dark">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">

          Are you sure?
        </div>
        <div class="modal-footer justify-content-right">
          <button type="button" id='modal-btn-yes' class="btn btn-dark">Yes</button>
          <button type="button" id="modal-btn-no" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- ------/.Confirm Model--------->

  <script src="<?= ASSETS_URL ?>js/pages/user-list.js"></script>