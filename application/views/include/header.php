<!DOCTYPE html>
<html>

<head>
    <title><?= $page_title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- PAGE SCRIPTS -->
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/adminLTE/css/adminlte.min.css">
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/adminLTE/css/custom.css">
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/pages/pagination.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="<?= ASSETS_URL ?>css/overlayScrollbars/OverlayScrollbars.min.css">
    <script src="<?= ASSETS_URL ?>js/jquery/jquery.min.js"></script>
    <script src="<?= ASSETS_URL ?>js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="<?= ASSETS_URL ?>js/adminLTE/adminlte.js"></script>
    <script src="<?= ASSETS_URL ?>js/adminLTE/demo.js"></script>
    <script src="<?= ASSETS_URL ?>js/bootstrap/bootstrap.min.js"></script>
    <!-- <script src="<?= ASSETS_URL ?>js/adminLTE/adminlte.min.js"></script> -->
    <script src="<?= ASSETS_URL ?>js/jquery-mousewheel/jquery.mousewheel.js"></script>
    <script src="<?= ASSETS_URL ?>js/overlayScrollbars/jquery.overlayScrollbars.min.js"></script>
    
    <script src="<?= ASSETS_URL ?>js/lib/ASPSnippets_Pager.min.js"></script>
    <script src="<?= ASSETS_URL ?>js/lib/RestAdapter.js"></script>
    <script src="<?= ASSETS_URL ?>js/lib/StringUtil.js"></script>

</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">