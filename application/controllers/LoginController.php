<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!isset($_SESSION['aa_user_id'])) {
            $this->load->view('login_view');
        } else {
            redirect('dashboard');
        }
            
    }

    public function checkLogin()
    {
        $reponse = [
            'flag' => 1,
            'message' => 'Ooh! Somthing happened terrible.',
            'data' => '',
        ];
        $username = htmlentities(strip_tags(trim($_POST['username'])));

        $loginInfo = $this->LoginModel->checkLogin($username);
        if (isset($loginInfo)) {
            $reponse = [
                'flag' => 0,
                'message' => 'success',
                'data' => $username,
            ];
        } else if ($loginInfo) {
            $reponse = [
                'flag' => 2,
                'message' => 'Set new password',
                'data' => $loginInfo,
                'id' => $loginInfo->UserId,
            ];
        } else {
            $reponse = [
                'flag' => 1,
                'message' => 'You have entered incorrect username or phone.',
                'data' => null,
            ];
        }
        echo json_encode($reponse);
    }

    public function loginAuthentication()
    {
        $reponse = [
            'flag' => 1,
            'message' => 'Ooh! Somthing happened terrible.',
            'data' => '',
        ];
        $username = htmlentities(strip_tags(trim($_POST['username'])));
        $password = htmlentities(strip_tags(trim($_POST['password'])));

        $encryptPassword = EncryptStr($password, ED_KEY);

        $loginInfo = $this->LoginModel->checkLoginAuthentication($username, $encryptPassword);
        if ($loginInfo) {

            $newdata = array(
                'aa_user_id' => '1',
                'aa_username_name' => 'admin',
                'aa_username' => $username
            );

            $this->session->set_userdata($newdata);

            $reponse = [
                'flag' => 0,
                'message' => 'success',
                'data' => $username
            ];
        } else {
            $reponse = [
                'flag' => 1,
                'message' => 'You have entered incorrect password.',
                'data' => null,
            ];
        }
        echo json_encode($reponse);
    }

    public function logout()
    {
        session_destroy();
        redirect('login');
    }

}
