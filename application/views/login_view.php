<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= ASSETS_URL ?>css/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= ASSETS_URL ?>css/adminLTE/css/adminlte.min.css">
  <!-- Custome style -->
  <link rel="stylesheet" href="<?= ASSETS_URL ?>css/adminLTE/css/custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="javascript:void(0);"><b>ADMIN</b>Panel</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form id='loingFrm'>
          <div class="input-group mb-3">
            <div class="input-group-append">
              <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" name='username' id='username' class="form-control" placeholder="Enter username or phone">
          </div>
          <div class="input-group mb-3" id='divPwd' style='display:none;'>
            <div class="input-group-append" style='width:40px;display: inline-block;'>
              <span class="input-group-text"><i class="fas fa-envelope"></i></span>
            </div>
            <input type="password" name='password' id='password' class="form-control input_user" style="display: inline-block;width: 220px;margin: 0px 0px 0 -4px;" placeholder="Password">
          </div>
          <div id='divConfirmPassword' style='display:none;'>
            <input type="password" name='confirm_password' id='confirm_password' class="form-control" placeholder="Confirm Password">
          </div>
          <div id='divOTP' style='display:none;'>
            <input type="number" name='otp' id='otp' min='1' maxlength='6' placeholder="OTP" onkeyup="if(/^\d{7}$/.test(this.value)) {this.value=this.value.slice(0,'6')}">
          </div>
          <div>
            <button type="button" id='loginFormNextButton' onclick=loginNextButton() class="btn btn-primary">Next</button>
          </div>
        </form>

      </div>
      <!-- /.login-card-body -->
      <div class="mt-4 mb-4">
        <div class="d-flex justify-content-center links">
          Don't have an account? <a href="#" class="ml-2">Sign Up</a>
        </div>
        <div class="d-flex justify-content-center links">
          <a class="underlineHover forgotLink" onclick=forgetPassword() href="javascript:void(0);">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- /.login-box -->

  <!-- jQuery -->
  <script src="<?= ASSETS_URL ?>js/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= ASSETS_URL ?>js/bootstrap/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= ASSETS_URL ?>js/adminLTE/adminlte.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= ASSETS_URL ?>js/adminLTE/adminlte.min.js"></script>
  <!-- custome login -->
  <script src="<?= ASSETS_URL ?>js/lib/RestAdapter.js"></script>
  <script src="<?= ASSETS_URL ?>js/lib/StringUtil.js"></script>
  <script src="<?= ASSETS_URL ?>js/adminLTE/custom.js"></script>
  <script src="<?= ASSETS_URL ?>js/pages/login-auth.js"></script>


</body>

</html>