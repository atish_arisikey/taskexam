// -------------Show popup msg---------------
function showSuccessMessage(message) {

    $(document).Toasts('create', {
        class: 'bg-success',
        position: 'topRight',
        autohide: true,
        delay: 4000,
        fixed: false,
        icon: 'fas fa-thumbs-up',
        title: message,
        // subtitle: 'Subtitle',
        // body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
    });
}

function showWarningMessage(message) {
    $(document).Toasts('create', {
        class: 'bg-warning',
        position: 'topRight',
        autohide: true,
        delay: 4000,
        fixed: false,
        icon: 'fas fa-exclamation',
        title: message,
        // subtitle: 'Subtitle',
        // body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
    });
}

function showErrorMessage(message) {
    $(document).Toasts('create', {
        class: 'bg-danger',
        position: 'topRight',
        autohide: true,
        delay: 4000,
        fixed: false,
        icon: 'fas fa-exclamation',
        title: message,
        // subtitle: 'Subtitle',
        // body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
    });
}

// -------------End show popup msg---------------