<?php
class UserModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insertData($fullName, $mobile, $emailId, $dateOfBirth)
    {
        $currentDateTime = date("Y-m-d H:i:s");
        $sql = "Insert into Users(FullName,Contact,EmailId,DateOfBirth,RegistrationDate,UserType'Status)
                Values('$fullName','$mobile','$emailId','$dateOfBirth','$currentDateTime ',2,1)";

        $this->db->query($sql);
        return $this->db->insert_id();
    }

    public function getAllData($page, $search, $filter_type, $from_date, $to_date,$sortBy,$sortType)
    {
        $pageSize = 20;
        $offset = $page * $pageSize;

        switch ($filter_type) {
            case 'all':
                $sql = "select user.UserId,user.FullName,user.Contact,user.EmailId,user.DateOfBirth,user.RegistrationDate,user.Status,
                ifnull((select Score from Scores score where user.UserId=score.UserId limit 1),-1) as Scores
                FROM Users user
                where (user.FullName LIKE '%$search%' 
                or user.Contact LIKE '%$search%'
                or user.EmailId LIKE '%$search%')
                and UserType=2
                order by";
                $sql .= (empty($sortBy))?" user.UserId desc":" user.$sortBy $sortType";
                $sql .=" LIMIT $offset,$pageSize";
                break;

            case 'date':
                $sql = "select user.UserId,user.FullName,user.Contact,user.EmailId,user.DateOfBirth,user.RegistrationDate,user.Status,
                ifnull((select Score from Scores score where user.UserId=score.UserId limit 1),-1) as Scores
                FROM Users user
                where (user.FullName LIKE '%$search%' 
                or user.Contact LIKE '%$search%'
                or user.EmailId LIKE '%$search%')
                and UserType=2
                and date(user.RegistrationDate) = '$from_date'
                order by";
                $sql .= (empty($sortBy))?" user.UserId desc":" user.$sortBy $sortType";
                $sql .=" LIMIT $offset,$pageSize";
                break;

            case 'today':
                $sql = "select user.UserId,user.FullName,user.Contact,user.EmailId,user.DateOfBirth,user.RegistrationDate,user.Status,
                ifnull((select Score from Scores score where user.UserId=score.UserId limit 1),-1) as Scores
                FROM Users user
                where (user.FullName LIKE '%$search%' 
                or user.Contact LIKE '%$search%'
                or user.EmailId LIKE '%$search%')
                and UserType=2
                and date(user.RegistrationDate) = CURDATE()
                order by";
                $sql .= (empty($sortBy))?" user.UserId desc":" user.$sortBy $sortType";
                $sql .=" LIMIT $offset,$pageSize";
                break;

            case 'weekly':
                $sql = "select user.UserId,user.FullName,user.Contact,user.EmailId,user.DateOfBirth,user.RegistrationDate,user.Status,
                ifnull((select Score from Scores score where user.UserId=score.UserId limit 1),-1) as Scores
                FROM Users user
                where (user.FullName LIKE '%$search%' 
                or user.Contact LIKE '%$search%'
                or user.EmailId LIKE '%$search%')
                and UserType=2
                and date(user.RegistrationDate) > '$from_date' -INTERVAL 7 day
                order by";
                $sql .= (empty($sortBy))?" user.UserId desc":" user.$sortBy $sortType";
                $sql .=" LIMIT $offset,$pageSize";
                break;

            case 'between':
                $sql = "select user.UserId,user.FullName,user.Contact,user.EmailId,user.DateOfBirth,user.RegistrationDate,user.Status,
                ifnull((select Score from Scores score where user.UserId=score.UserId limit 1),-1) as Scores
                FROM Users user
                where (user.FullName LIKE '%$search%' 
                or user.Contact LIKE '%$search%'
                or user.EmailId LIKE '%$search%')
                and UserType=2
                and date(user.RegistrationDate) between '$from_date' and '$to_date'
                order by";
                $sql .= (empty($sortBy))?" user.UserId desc":" user.$sortBy $sortType";
                $sql .=" LIMIT $offset,$pageSize";
                break;

            default:
                $sql = "select user.UserId,user.FullName,user.Contact,user.EmailId,user.DateOfBirth,user.RegistrationDate,user.Status,
                ifnull((select Score from Scores score where user.UserId=score.UserId limit 1),-1) as Scores
                FROM Users user
                where (user.FullName LIKE '%$search%' 
                or user.Contact LIKE '%$search%'
                or user.EmailId LIKE '%$search%')
                and UserType=2";
                $sql .= (empty($sortBy))?" user.UserId desc":" user.$sortBy $sortType
                order by";
                $sql .=" LIMIT $offset,$pageSize";
                break;
        }

        $results = $this->db->query($sql)->result_array();

        return $results;
    }

    public function getListCount($search, $filter_type, $from_date, $to_date)
    {
        switch ($filter_type) {
            case 'all':
                $sql = "select Count(*) as Count 
                FROM Users 
                where (FullName LIKE '%$search%' 
                or Contact LIKE '%$search%'
                or EmailId LIKE '%$search%')
                and UserType=2
                order by UserId desc";
                break;

            case 'date':
                $sql = "select Count(*) as Count 
                FROM Users 
                where (FullName LIKE '%$search%' 
                or Contact LIKE '%$search%'
                or EmailId LIKE '%$search%')
                and UserType=2
                and date(RegistrationDate) = '$from_date'
                order by UserId desc";
                break;

            case 'today':
                $sql = "select Count(*) as Count 
                FROM Users 
                where (FullName LIKE '%$search%' 
                or Contact LIKE '%$search%'
                or EmailId LIKE '%$search%')
                and UserType=2
                and date(RegistrationDate) = CURDATE()
                order by UserId desc";
                break;

            case 'weekly':
                $sql = "select Count(*) as Count 
                FROM Users 
                where (FullName LIKE '%$search%' 
                or Contact LIKE '%$search%'
                or EmailId LIKE '%$search%')
                and UserType=2
                and date(RegistrationDate) > '$from_date' -INTERVAL 7 day
                order by UserId desc";
                break;

            case 'between':
                $sql = "select Count(*) as Count 
                FROM Users 
                where (FullName LIKE '%$search%' 
                or Contact LIKE '%$search%'
                or EmailId LIKE '%$search%')
                and UserType=2
                and date(RegistrationDate) between '$from_date' and '$to_date'
                order by UserId desc";
                break;

            default:
                $sql = "select Count(*) as Count 
                FROM Users 
                where (FullName LIKE '%$search%' 
                or Contact LIKE '%$search%'
                or EmailId LIKE '%$search%')
                and UserType=2
                order by UserId desc";
                break;
        }

        $resultCount = $this->db->query($sql)->row_array()['Count'];

        return $resultCount;
    }

    public function updateData($id, $fullName, $mobile, $emailId, $dateOfBirth)
    {
        $sql = "Update Users Set ";

        $updateStatement = "";

        if (!empty($fullName)) {
            if (!empty($updateStatement)) {
                $updateStatement .= ", ";
            }
            $updateStatement .= " FullName='$fullName'";
        }

        if (!empty($mobile)) {

            if (!empty($updateStatement)) {
                $updateStatement .= ", ";
            }

            $updateStatement .= " Contact='$mobile'";
        }

        if (!empty($emailId)) {
            if (!empty($updateStatement)) {
                $updateStatement .= ", ";
            }
            $updateStatement .= " EmailId='$emailId'";
        }

        if (!empty($dateOfBirth)) {

            if (!empty($updateStatement)) {
                $updateStatement .= ", ";
            }

            $updateStatement .= " DateOfBirth='$dateOfBirth'";
        }

        $result = true;
        if (!empty($updateStatement)) {
            $sql .= $updateStatement . " where UserId=$id";
            $result = $this->db->query($sql);
        } else {
            $result = false;
        }

        return $result;
    }

    public function deleteData($id)
    {
        $sql = "Delete from Scores where UserId=$id";
        $this->db->query($sql);

        $sql = "Delete from Users where UserId=$id";
        return $this->db->query($sql);
    }

    public function getUserDetails($username)
    {
        $userDetails = $this->db->query("select user.*,
        (select score.Score from Scores score where score.UserId=user.UserId limit 1) as Score
        from Users user
        where (user.EmailId ='" . $username . "' or user.Contact='" . $username . "')
        and user.UserType=2")->row();
        return $userDetails;
    }

    public function insertScore($id, $score)
    {
        $currentDateTime = date("Y-m-d H:i:s");
        $sql = "Insert into Scores(UserId,Score,Date)
                Values($id,$score,'$currentDateTime')";

        return $this->db->query($sql);
    }
}
