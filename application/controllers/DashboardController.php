<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        isLoggedIn();
    }

    public function index()
    {
        $data['page_title'] = 'Dashboard | EZ Store India';
        $data['page'] = 'dashboard_view';
        $this->load->view('include/template', $data);
    }
}
