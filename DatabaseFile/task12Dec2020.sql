-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: task12Dec2020
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `UserId` bigint(255) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(160) DEFAULT NULL,
  `Contact` varchar(160) DEFAULT NULL,
  `EmailId` varchar(160) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `RegistrationDate` datetime DEFAULT NULL,
  `Status` int(2) DEFAULT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'admin','8208483013','admin@gmail.com','2020-10-26','2020-10-26 10:53:52',1),(29,'Jack Lui','9838890188','jack@gmail.com','1994-01-13','2020-11-03 10:17:50',1),(30,'Susen McLane','8883471048','susen@gmail.com','1992-03-04','2020-11-03 10:58:46',1),(31,'Lucy Smith','8983334682','lucy@gmail.com','1998-03-05','2020-11-03 18:26:58',0),(37,'Stanley Smith','8892357293','stanleyfood@gmail.com','0000-00-00','2020-11-09 11:09:15',0),(38,'Luck McLane','8892100347','gofood@gmail.com','0000-00-00','2020-11-09 11:13:38',0),(39,'Shown Willias','9210038462','shorcutfood@gmail.com','0000-00-00','2020-11-09 11:15:47',1),(41,'Lucky','9888372803','kfood@gmail.com','0000-00-00','2020-11-10 21:11:36',0),(42,'Johnethan Smith','9963104901','lifemedical@gmail..com','0000-00-00','2020-11-11 17:21:40',1),(43,'Magnum Moore','9983283468','greatfruit@gmail.com','0000-00-00','2020-11-12 01:05:11',0),(44,'Magnum Moore','9623608964','karanagarwal398@gmail.com','0000-00-00','2020-11-12 01:05:11',1),(45,'Kat Wilson','8892836489','kat@gmail.com','0000-00-00','2020-12-09 19:09:35',1),(46,'Rose Moss','8837211049','rose@gmail.com','0000-00-00','2020-12-09 19:36:17',1),(47,'Kita Loo','8812340947','kita@gmail.com','0000-00-00','2020-12-09 19:42:53',1),(48,'rita moore','8827234678','rita@gmail.com','0000-00-00','2020-12-09 19:48:41',1),(49,'maggi shock','8883948758','maggi@gmail.com','0000-00-00','2020-12-09 19:51:58',1),(50,'groom broom','8810002837','groom@gmail.com','0000-00-00','2020-12-09 19:53:06',1),(51,'aro','8832938648','aro@gmail.com','0000-00-00','2020-12-09 20:02:45',1),(52,'astro','8111039571','astro@gmail.com','0000-00-00','2020-12-09 22:42:32',1),(53,'Nancy','8983389801','nancy@gmail.com','0000-00-00','2020-12-09 22:45:23',1),(54,'costa','8998731234','costa@gmail.com','0000-00-00','2020-12-09 22:49:29',1),(55,'rore','8821230987','rore@gmail.com','0000-00-00','2020-12-09 22:51:19',1),(57,'root','9003827489','root@gmail.com','0000-00-00','2020-12-09 22:56:56',1),(58,'mose','8888999023','mose@gmail.com','0000-00-00','2020-12-09 23:02:21',1),(59,'akira','8888999021','akira@gmail.com','0000-00-00','2020-12-09 23:04:24',1),(60,'suru','2338638838','suru@gmail.com','0000-00-00','2020-12-09 23:06:42',1),(61,'door','8829902192','door@gmail.com','0000-00-00','2020-12-09 23:09:16',1),(62,'oreo','8889000000','oreo@gmail.com','0000-00-00','2020-12-09 23:10:14',1),(63,'token','8989832384','token@gmail.com','0000-00-00','2020-12-09 23:23:44',1),(64,'macro','8382838888','macro@gmail.com','0000-00-00','2020-12-09 23:33:42',1),(65,'kiso','8998233338','kiso@gmail.com','0000-00-00','2020-12-10 11:18:22',1),(66,'Danial Redcliff','8999093875','danial@gmail.com','0000-00-00','2020-12-10 11:22:40',1),(69,'Rocky','8999092387','rocky@gmail.com','0000-00-00','2020-12-10 14:22:19',1),(70,'Torror','8898222221','torror@gmail.com','0000-00-00','2020-12-10 14:31:37',1),(71,'Drona','9998128394','drona@gmail.com','1993-07-07','2020-12-10 17:57:39',1),(72,'Carron','8382833486','carron@gmail.com','1996-08-07','2020-12-12 17:43:04',1),(73,'Oscar','8839288389','oscar@gmail.com','2020-01-01','2020-12-12 22:05:50',1);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-12 22:24:09
